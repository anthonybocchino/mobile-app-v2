import { View, Image } from 'react-native'
/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */
export default function () {
  const { Layout, Images } = useTheme()
  return {
    logo: {
      uri:
      Images.logo
    },
  }
}
